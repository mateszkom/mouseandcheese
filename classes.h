enum keyboard { UP, DOWN, LEFT, RIGHT };
enum chooseObject { mouse, wedrowiecW, wallW, cactusW, cheeseW, serduszkoW, bonusW, holeW, seedW, randommouseW, enemyW};

enum status { MENU, NEW_GAME, RANK, OPTIONS, EXIT };

//bool keys[4] = { false,false,false,false };

int level = 22;
const float FPS = 60;
const size_t WIDTH = 1280;
const size_t HEIGHT = 720;

int liczbaserc = 5;

int licznikserow = 0;
int licznikbonusow = 0;
int licznikseedow = 0;

int serplus0 = 0;
int serplus1 = 0;
int serplus2 = 0;
int serplus3 = 0;
int serplus4 = 0;
int serplus5 = 0;
int serplus6 = 0;
int serplus7 = 0;
int serplus8 = 0;
int serplusm9 = 0;
int serplusm8 = 0;
int serplusm7 = 0;
int serplusm6 = 0;
int serplusm5 = 0;
int serplusm4 = 0;
int serplusm16 = 0;
int serplusm26 = 0;
int serplusm25 = 0;
int serplusm24 = 0;
int serplusm34 = 0;
int serplus13 = 0;
int serplus14 = 0;
int serplus15 = 0;
int serplus16 = 0;
int serplus22 = 0;
int serplus23 = 0;
int serplus24 = 0;
int serplus25 = 0;
int serplus32 = 0;
int serplus42 = 0;
int serplus43 = 0;
int serplus44 = 0;
int serplus45 = 0;
int serplus46 = 0;
int serplus47 = 0;
int serplus37 = 0;



int bonusplus0 = 0;
int bonusplus1 = 0;
int bonusplus2 = 0;
int bonusplus3 = 0;
int bonusplus4 = 0;
int bonusplus5 = 0;
int bonusplus6 = 0;
int bonusplus7 = 0;
int bonusplus8 = 0;
int bonusplusm9 = 0;
int bonusplusm8 = 0;
int bonusplusm7 = 0;
int bonusplusm6 = 0;
int bonusplusm5 = 0;
int bonusplusm4 = 0;
int bonusplusm16 = 0;
int bonusplusm26 = 0;
int bonusplusm25 = 0;
int bonusplusm24 = 0;
int bonusplusm34 = 0;
int bonusplus13 = 0;
int bonusplus14 = 0;
int bonusplus15 = 0;
int bonusplus16 = 0;
int bonusplus22 = 0;
int bonusplus23 = 0;
int bonusplus24 = 0;
int bonusplus25 = 0;
int bonusplus32 = 0;
int bonusplus42 = 0;
int bonusplus43 = 0;
int bonusplus44 = 0;
int bonusplus45 = 0;
int bonusplus46 = 0;
int bonusplus47 = 0;
int bonusplus37 = 0;


int seedplus0 = 0;
int seedplus1 = 0;
int seedplus2 = 0;
int seedplus3 = 0;
int seedplus4 = 0;
int seedplus5 = 0;
int seedplus6 = 0;
int seedplus7 = 0;
int seedplus8 = 0;
int seedplusm9 = 0;
int seedplusm8 = 0;
int seedplusm7 = 0;
int seedplusm6 = 0;
int seedplusm5 = 0;
int seedplusm4 = 0;
int seedplusm16 = 0;
int seedplusm26 = 0;
int seedplusm25 = 0;
int seedplusm24 = 0;
int seedplusm34 = 0;
int seedplus13 = 0;
int seedplus14 = 0;
int seedplus15 = 0;
int seedplus16 = 0;
int seedplus22 = 0;
int seedplus23 = 0;
int seedplus24 = 0;
int seedplus25 = 0;
int seedplus32 = 0;
int seedplus42 = 0;
int seedplus43 = 0;
int seedplus44 = 0;
int seedplus45 = 0;
int seedplus46 = 0;
int seedplus47 = 0;
int seedplus37 = 0;


int kolizja = 0;



//
int poz_enemy;
bool move_enemy = false;
int xwilka1 = 400;


class enemy
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};


class character
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
	int frame = 0;
	int direction;
};

float SPEED = 20.0;


class wedrowiec
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};



class wall
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};


//kaktus blokuje i zabiera serduszko i cofa na start
class cactus
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};

class hole
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};

class cheese
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};

class serduszko
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};


class bonus
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};

class seed
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};


class randommouse
{
public:
	ALLEGRO_BITMAP * bitmap = NULL;
	float coord_x;
	float coord_y;
};



