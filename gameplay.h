
void gameplay
(
	ALLEGRO_DISPLAY **window,
	ALLEGRO_EVENT_QUEUE **queue,
	ALLEGRO_FONT **gamefont,
	int *idk
)
{
	srand(time(NULL));
	ALLEGRO_BITMAP *przeciwnik = al_load_bitmap("enemy.png");

	enemy *enemyP;
	character* person;
	wedrowiec *wedrowiecP;
	wall *wallP;
	cactus *cactusP;
	cheese *cheeseP;
	serduszko *serduszkoP;
	bonus *bonusP;
	hole *holeP;
	ALLEGRO_BITMAP *backgroundmap = NULL;
	seed *seedP;
	randommouse *randommouseP;
	ALLEGRO_BITMAP *cheesebar = NULL;
	ALLEGRO_BITMAP *shopmouse = NULL;
	ALLEGRO_BITMAP *fullheart = NULL;
	ALLEGRO_BITMAP *seedbar = NULL;
	ALLEGRO_BITMAP *dymeksklep = NULL;




	//kalokowanie pamieci

		enemyP = (enemy *)calloc(2, sizeof(enemy));
		if (enemyP == NULL)
		{
			cout << "failed to allocate memory to eneemyyy bitmap" << endl;
			return;
		}

		person = (character *)calloc(2, sizeof(character));
		if (person == NULL)
		{
			cout << "failed to allocate memory to person bitmap" << endl;
			return;
		}

		wedrowiecP = (wedrowiec *)calloc(2, sizeof(wedrowiec));
		if (wedrowiecP == NULL)
		{
			cout << "failed to allocate memory to wedrowiecP bitmap" << endl;
			return;
		}



		wallP = (wall *)calloc(2, sizeof(wall));
		if (wallP == NULL)
		{
			cout << "failed to allocate memory to wall bitmap" << endl;
			return;
		}

		cactusP = (cactus *)calloc(2, sizeof(cactus));
		if (cactusP == NULL)
		{
			cout << "failed to allocate memory to cactus bitmap" << endl;
			return;
		}

		cheeseP = (cheese *)calloc(2, sizeof(cheese));
		if (cheeseP == NULL)
		{
			cout << "failed to allocate memory to cheese bitmap" << endl;
			return;
		}
		
		serduszkoP = (serduszko *)calloc(2, sizeof(serduszko));
		if (serduszkoP == NULL)
		{
			cout << "failed to allocate memory to serduszko bitmap" << endl;
			return;
		}

		bonusP = (bonus *)calloc(2, sizeof(bonus));
		if (bonusP == NULL)
		{
			cout << "failed to allocate memory to bonus bitmap" << endl;
			return;
		}

		holeP = (hole *)calloc(2, sizeof(hole));
		if (holeP == NULL)
		{
			cout << "failed to allocate memory to hole bitmap" << endl;
			return;
		}

		seedP = (seed *)calloc(2, sizeof(seed));
		if (seedP == NULL)
		{
			cout << "failed to allocate memory to seed bitmap" << endl;
			return;
		}

		randommouseP = (randommouse *)calloc(2, sizeof(randommouse));
		if (randommouseP == NULL)
		{
			cout << "failed to allocate memory to randommouse bitmap" << endl;
			return;
		}

		






	if (!initGameplay(&enemyP[enemyW].bitmap, &person[mouse].bitmap, &wedrowiecP[wedrowiecW].bitmap, &wallP[wallW].bitmap, &cactusP[cactusW].bitmap,
		&cheeseP[cheeseW].bitmap, &serduszkoP[serduszkoW].bitmap, &bonusP[bonusW].bitmap, &holeP[holeW].bitmap, &seedP[seedW].bitmap, &randommouseP[randommouseW].bitmap, &backgroundmap,
		&cheesebar, &shopmouse, &fullheart, &seedbar, &dymeksklep))
	{
		cout << "initgameplay crashed" << endl;
		return;
	}


	bool collision[2][2] = { false, false, false, false };
	bool refresh = true;
	bool key[1][4] = { false, false, false, false };
	int direction[2] = { 0,0 };//

	person[mouse].coord_x = WIDTH / 24;
	person[mouse].coord_y = HEIGHT / 2.5;

	al_set_target_bitmap(backgroundmap);
	//al_clear_to_color(al_map_rgb(20, 20, 20));

	al_set_target_bitmap(al_get_backbuffer(*window));


	while (true)
	{
		ALLEGRO_EVENT event;

		al_wait_for_event(*queue, &event);
		if (event.type == ALLEGRO_EVENT_TIMER)
		{
			movingProcess(&key[mouse][0], &person[mouse]);
			refresh = true;

				







		}

		if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			*idk = EXIT;
			break;
		}
		if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			keyState(&key[mouse][0], true, event, mouse);
			if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
			{
				*idk = EXIT;
				break;
			}
		}
		if (event.type == ALLEGRO_EVENT_KEY_UP)
		{
			keyState(&key[mouse][0], false, event, mouse);
		}






		//drawing
		if (refresh && al_event_queue_is_empty(*queue))
		{

			refresh = false;
			//al_clear_to_color(al_map_rgb(50, 50, 50));
			al_draw_bitmap(backgroundmap, 0, 0, 0);


			//drawing
			if (level == 0)
				drawlevelplus0(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 1)
				drawlevelplus1(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 2)
				drawlevelplus2(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 3)
				drawlevelplus3(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 4)
				drawlevelplus4(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 5)
				drawlevelplus5(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 6)
				drawlevelplus6(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 7)
				drawlevelplus7(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 8)
				drawlevelplus8(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);



			if (level == -9)
			{
				drawlevelplusm9(&dymeksklep, &wedrowiecP[wedrowiecW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &serduszkoP[serduszkoW]);
				al_draw_bitmap(shopmouse, 80, 540, 0);
			}

			if (level == -8)
				drawlevelplus8(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -7)
				drawlevelplusm7(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -6)
				drawlevelplusm6(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -5)
				drawlevelplusm5(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -4)
				drawlevelplusm4(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -16)
				drawlevelplusm16(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -24)
				drawlevelplusm24(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -25)
				drawlevelplusm25(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -26)
				drawlevelplusm26(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == -34)
				drawlevelplusm34(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			

			
			if (level == 13 )
				drawlevelplus13(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);
			
			if (level == 14)
				drawlevelplus14(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);
		
			if (level == 15)
				drawlevelplus15(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 16)
				drawlevelplus16(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 22)
				drawlevelplus22(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 23)
				drawlevelplus23(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 24)
				drawlevelplus24(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 25)
				drawlevelplus25(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 32)
				drawlevelplus32(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 42)
				drawlevelplus42(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 43)
				drawlevelplus43(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 44)
				drawlevelplus44(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 45)
				drawlevelplus45(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 46)
				drawlevelplus46(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);
		
			if (level == 47)
				drawlevelplus47(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);

			if (level == 37)
				drawlevelplus37(&enemyP[enemyW], &wallP[wallW], &person[mouse], &key[mouse][0], &cactusP[cactusW], &holeP[holeW], &cheeseP[cheeseW], &bonusP[bonusW], &seedP[seedW]);
















			clock_t czas;
			double czass;
			czas = clock();
			czass = (double)(czas) / CLOCKS_PER_SEC;
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), WIDTH / 1.5, 680, ALLEGRO_ALIGN_CENTER, " %2.2fs ", czass);
			drawCharacter(&key[mouse][0], &person[mouse]);





			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), WIDTH / 2, 5, ALLEGRO_ALIGN_CENTRE, "x= %f y= %f", person[mouse].coord_x, person[mouse].coord_y);
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), WIDTH / 3, 5, ALLEGRO_ALIGN_CENTRE, "ser = %i", licznikserow);
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), WIDTH / 4.4, 5, ALLEGRO_ALIGN_CENTRE, "bonus = %i", licznikbonusow);
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), WIDTH / 2, 30, ALLEGRO_ALIGN_CENTRE, "level = %i", level);



			//liczenie serw
			al_draw_bitmap(cheesebar, 1120, 680, 0);
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), 1100, 680, ALLEGRO_ALIGN_CENTRE, "%i/5", licznikserow);

			//liczenie seedow
			al_draw_bitmap(seedbar, 1188, 680, 0);
			al_draw_textf(*gamefont, al_map_rgb(255, 255, 255), 1180, 680, ALLEGRO_ALIGN_CENTRE, "%i", licznikseedow);



			


			//ilosc serduszek
			for (int i = 0; i < liczbaserc; i++)
				al_draw_bitmap(fullheart, 40 + i * 40, 680, 0);


			//warunek przegranej
			//if (liczbaserc == 0)
			//al_draw_bitmap(dymeksklep, 0, 0, 0);




			al_flip_display();

		}
	} //koniec while

	  //destroyGameplay(&person[mouse].bitmap, &wallP[wallW].bitmap, &cactusP[cactusW].bitmap, &cheese, &bonus, &hole, &person, &backgroundmap);

	return;

}//koniec void gameplay
