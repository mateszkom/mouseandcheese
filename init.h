using namespace std;

bool initGenerally
(
	ALLEGRO_DISPLAY **window,
	ALLEGRO_EVENT_QUEUE **queue,
	ALLEGRO_TIMER **timer,
	ALLEGRO_FONT **gamefont
)
{
	if (!al_init())
	{
		cout << "Failed to initialize allegro!" << endl;
		exit(1);
	}

	if (!al_install_keyboard())
	{
		cout << "Failed to initialize keyboard!" << endl;
		exit(1);
	}

	if (!al_init_image_addon())
	{
		cout << "Failed to initialize al_init_image_addon!" << endl;
		exit(1);
	}

	if (!al_install_mouse())
	{
		cout << "Failed to initialize mouse!" << endl;
		exit(1);
	}
	if (!al_init_font_addon())
	{
		cout << "Failed to initialize al_init_font_addon!" << endl;
		exit(1);
	}

	if (!al_init_ttf_addon())
	{
		cout << "Failed to initialize al_init_ttf_addon!" << endl;
		exit(1);
	}

	if (!al_init_primitives_addon())
	{
		cout << "Failed to initialize al_init_primitives_addon!" << endl;
		exit(1);
	}


	*window = al_create_display(WIDTH, HEIGHT);
	if (!*window)
	{
		cout << "Failed to initialize window!" << endl;
		exit(1);
	}



	*queue = al_create_event_queue();

	if (!*queue)
	{
		cout << "Failed with making queue" << endl;

		exit(1);
	}

	*timer = al_create_timer(1.0 / FPS);
	if (!*timer)
	{
		cout << "Failed with making timer" << endl;

		exit(1);
	}

	*gamefont = al_load_ttf_font("fonts/arial.ttf", 20, 0);
	if (!*gamefont)
	{
		cout << "Failed to load font!" << endl;

		exit(1);
	}
	return true;
}

bool initGameplay
(
	ALLEGRO_BITMAP **enemy,
	ALLEGRO_BITMAP **mouse,
	ALLEGRO_BITMAP **wedrowiec,
	ALLEGRO_BITMAP **wall,
	ALLEGRO_BITMAP **cactus,
	ALLEGRO_BITMAP **cheese,
	ALLEGRO_BITMAP **serduszko,
	ALLEGRO_BITMAP **bonus,
	ALLEGRO_BITMAP **hole,
	ALLEGRO_BITMAP **seed,
	ALLEGRO_BITMAP **randommouse,
	ALLEGRO_BITMAP **backgroundmap,
	ALLEGRO_BITMAP **cheesebar,
	ALLEGRO_BITMAP **shopmouse,
	ALLEGRO_BITMAP **fullheart,
	ALLEGRO_BITMAP **seedbar,
	ALLEGRO_BITMAP **dymeksklep

)
{

	*enemy = al_load_bitmap("graphics/enemy.png");
	if (!*enemy)
	{
		cout << "failed to load enemy bitmap" << endl;
		exit(1);
	}

	*mouse = al_load_bitmap("graphics/mouse128.png");
	if (!*mouse)
	{
		cout << "failed to load mouse bitmap" << endl;
		exit(1);
	}

	*wedrowiec = al_load_bitmap("graphics/wedrowiec1.png");
	if (!*wedrowiec)
	{
		cout << "failed to load wedrowiec bitmap" << endl;
		exit(1);
	}


	*wall = al_load_bitmap("graphics/wall.png");
	if (!*wall)
	{
		cout << "failed to load wall bitmap" << endl;
		exit(1);
	}

	*cactus = al_load_bitmap("graphics/cactus.png");
	if (!*cactus)
	{
		cout << "failed to load cactus bitmap" << endl;
		exit(1);
	}

	*cheese = al_load_bitmap("graphics/cheese.png");
	if (!*cheese)
	{
		cout << "failed to load cheese bitmap" << endl;
		exit(1);
	}
	
	
	*serduszko = al_load_bitmap("graphics/fullheart.png");
	if (!*serduszko)
	{
		cout << "failed to load serduszko bitmap" << endl;
		exit(1);
	}

	*bonus = al_load_bitmap("graphics/bonus.png");
	if (!*bonus)
	{
		cout << "failed to load bonus bitmap" << endl;
		exit(1);
	}

	*hole = al_load_bitmap("graphics/hole.png");
	if (!*hole)
	{
		cout << "failed to load hole bitmap" << endl;
		exit(1);
	}

	*backgroundmap = al_load_bitmap("graphics/background2.png");
	if (!*backgroundmap)
	{
		cout << "failed to load background bitmap" << endl;
		exit(1);
	}
	/*
	*cat = al_load_bitmap("graphics/cat1.png");
	if (!*cat)
	{
	cout << "failed to load cat bitmap" << endl;
	exit(1);
	}
	*/
	*seed = al_load_bitmap("graphics/seed.png");
	if (!*seed)
	{
		cout << "failed to load seed bitmap" << endl;
		exit(1);
	}

	*randommouse = al_load_bitmap("graphics/randommouse.png");
	if (!*randommouse)
	{
		cout << "failed to load randommouse bitmap" << endl;
		exit(1);
	}

	*cheesebar = al_load_bitmap("graphics/cheesebar.png");
	if (!*cheesebar)
	{
		cout << "failed to load cheesebar bitmap" << endl;
		exit(1);
	}

	*shopmouse = al_load_bitmap("graphics/shopmouse.png");
	if (!*shopmouse)
	{
		cout << "failed to load shopmouse bitmap" << endl;
		exit(1);
	}

	*fullheart = al_load_bitmap("graphics/fullheart.png");
	if (!*fullheart)
	{
		cout << "failed to load fullheart bitmap" << endl;
		exit(1);
	}


	*seedbar = al_load_bitmap("graphics/seedbar.png");
	if (!*seedbar)
	{
		cout << "failed to load seedbar bitmap" << endl;
		exit(1);
	}

	*dymeksklep = al_load_bitmap("graphics/dymeksklep.png");
	if (!*dymeksklep)
	{
		cout << "failed to load dymeksklep bitmap" << endl;
		exit(1);
	}

	return true;
}

void destroyGenerally
(
	ALLEGRO_DISPLAY **window,
	ALLEGRO_EVENT_QUEUE **queue,
	ALLEGRO_TIMER **timer,
	ALLEGRO_FONT **gamefont
)
{
	al_destroy_event_queue(*queue);
	al_destroy_timer(*timer);
	al_destroy_display(*window);
	al_destroy_font(*gamefont);
}

/*
void destroyGameplay
(
ALLEGRO_BITMAP **mouse,
ALLEGRO_BITMAP **wall,
ALLEGRO_BITMAP **cactus,
ALLEGRO_BITMAP **cheese,
ALLEGRO_BITMAP **bonus,
ALLEGRO_BITMAP **hole,
character **person,
ALLEGRO_BITMAP **background
)
{
al_destroy_bitmap(*mouse);
al_destroy_bitmap(*wall);
al_destroy_bitmap(*cactus);
al_destroy_bitmap(*cheese);
al_destroy_bitmap(*bonus);
al_destroy_bitmap(*hole);
free(*person);
al_destroy_bitmap(*background);
}
*/

void initEventSource
(
	ALLEGRO_DISPLAY **window,
	ALLEGRO_EVENT_QUEUE **queue,
	ALLEGRO_TIMER **timer
)
{
	al_register_event_source(*queue, al_get_display_event_source(*window));
	al_register_event_source(*queue, al_get_timer_event_source(*timer));
	al_register_event_source(*queue, al_get_keyboard_event_source());
	al_register_event_source(*queue, al_get_mouse_event_source());
}

bool initMenu
(
	ALLEGRO_BITMAP **title,
	ALLEGRO_FONT **menufont
)
{
	*title = al_load_bitmap("graphics/ekran.png");
	if (!*title)
	{
		cout << "failed to load title menu bitmap" << endl;
		exit(1);
	}

	*menufont = al_load_ttf_font("fonts/Gobold Uplow.ttf", 40, 0);
	if (!*menufont)
	{
		cout << "failed to load title menu font" << endl;
		exit(1);
	}

	return true;
}

void destroyMenu
(
	ALLEGRO_BITMAP **title,
	ALLEGRO_FONT **menufont
)
{
	al_destroy_bitmap(*title);
	al_destroy_font(*menufont);
}
