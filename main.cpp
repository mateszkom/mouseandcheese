#include "biblioteki.h"
#include "classes.h"
#include "init.h"
#include "character.h"
#include "menu.h"
#include "drawing.h"
#include "gameplay.h"
#include "enemy.h"




int main(int argc, char **argv)
{
	ALLEGRO_DISPLAY *window = NULL;
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *gamefont = NULL;


	if (!initGenerally
	(
		&window,
		&queue,
		&timer,
		&gamefont
	))
	{
		return EXIT_FAILURE;
	}
		
	int status = 1; //0 to menu, 1 gameplay

	initEventSource(&window, &queue, &timer);

	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

	al_start_timer(timer);

	while (true)
	{

		if (status == MENU)
		{
			menu(&queue, &window, &status);
		}

		if (status == NEW_GAME)
		{
			gameplay(&window, &queue, &gamefont, &status);
		}

		if (status == EXIT)
		{
			return EXIT_FAILURE;
		}
		
	}


	destroyGenerally(&window, &queue, &timer, &gamefont);

	return EXIT_SUCCESS;
}