//draw character dziala
void drawCharacter
(
	bool *key,
	character *character
)
{
	if ((*(key + RIGHT)) && !(*(key + UP)) && !(*(key + DOWN)))
	{
		al_draw_bitmap_region((*character).bitmap, (*character).frame, 384, 128, 128, (*character).coord_x, (*character).coord_y, 0);
	}
	if ((*(key + LEFT)) && !(*(key + UP)) && !(*(key + DOWN)))
	{
		al_draw_bitmap_region((*character).bitmap, (*character).frame, 128, 128, 128, (*character).coord_x, (*character).coord_y, 0);
	}
	if (*(key + UP))
	{
		al_draw_bitmap_region((*character).bitmap, (*character).frame, 256, 128, 128, (*character).coord_x, (*character).coord_y, 0);
	}
	if (*(key + DOWN))
	{
		al_draw_bitmap_region((*character).bitmap, (*character).frame, 0, 128, 128, (*character).coord_x, (*character).coord_y, 0);
	}
	if (!*(key + RIGHT) && !*(key + LEFT) && !*(key + UP) && !*(key + DOWN))
	{
		al_draw_bitmap_region((*character).bitmap, 0, 0, 128, 128, (*character).coord_x, (*character).coord_y, 0);
	}
	(*character).frame += 128;
	if (((*character).frame == 384) || (!*(key + RIGHT) && !*(key + LEFT) && !*(key + UP) && !*(key + DOWN)))
	{
		(*character).frame = 0;
	}
}


//key state dziala
void keyState
(
	bool *key,
	bool direction,
	ALLEGRO_EVENT event,
	int mouse
)
{

	if (!mouse)
	{
		switch (event.keyboard.keycode)
		{
		case ALLEGRO_KEY_A:
			*(key + LEFT) = direction;
			break;
		case ALLEGRO_KEY_D:
			*(key + RIGHT) = direction;
			break;
		case ALLEGRO_KEY_W:
			*(key + UP) = direction;
			break;
		case ALLEGRO_KEY_S:
			*(key + DOWN) = direction;
			break;
		}
	}

}


//moving proccess dziala
void movingProcess
(
	bool *key,
	character *character
)
{
	//poruszanie sie w danych kierunkach
	if (*(key + LEFT) && ((*character).coord_x >= -128-SPEED))
	{
		(*character).coord_x -= SPEED;
	}
	if (*(key + RIGHT) && ((*character).coord_x <= 1280-SPEED))
	{
		(*character).coord_x += SPEED;
	}
	if (*(key + UP) && ((*character).coord_y >= -128- SPEED))
	{
		(*character).coord_y -= SPEED;
	}
	if (*(key + DOWN) && ((*character).coord_y <= 720-SPEED))
	{
		(*character).coord_y += SPEED;
	}
	/*
	//ser++
	if
		(
		(!((*character).coord_x < 914 || (*character).coord_x - 64 > 914 ||
			(*character).coord_y < 114 || (*character).coord_y - 64 > 114))
			)
	{
		serplus++;
	}

	if
		(
		(!((*character).coord_x < 755 || (*character).coord_x - 64 > 755 ||
			(*character).coord_y < 300 || (*character).coord_y - 64 > 300))

			)
	{
		bonusplus++;
		SPEED += 0.5;
	}

	
	*/


}



