void lightKey
(
	ALLEGRO_EVENT *event,
	ALLEGRO_BITMAP **key,
	ALLEGRO_DISPLAY **window,
	int multiply
)
{
	//jezeli najezdzamy na cos to czerwony, jak nie to szary
	if (!((*event).mouse.x > WIDTH / 2 - al_get_bitmap_width(*key) / 2 && (*event).mouse.x < WIDTH / 2 - al_get_bitmap_width(*key) / 2 + 250 &&
		(*event).mouse.y > HEIGHT - HEIGHT / 10 * multiply && (*event).mouse.y < HEIGHT - HEIGHT / 10 * multiply + 65))
	{
		al_set_target_bitmap(*key);
		al_clear_to_color(al_map_rgb(20, 20, 20)); //szary
		al_set_target_bitmap(al_get_backbuffer(*window));
	}
	else
	{
		al_set_target_bitmap(*key);
		al_clear_to_color(al_map_rgb(255, 0, 0)); //red
		al_set_target_bitmap(al_get_backbuffer(*window));
	}
}


void menuOptions
(
	ALLEGRO_EVENT *event,
	ALLEGRO_BITMAP **key,
	int multiply,
	int *idk
)
{
	if (((*event).mouse.x > WIDTH / 2 - al_get_bitmap_width(*key) / 2 && (*event).mouse.x < WIDTH / 2 - al_get_bitmap_width(*key) / 2 + 300 &&
		(*event).mouse.y > HEIGHT - HEIGHT / 10 * multiply && (*event).mouse.y < HEIGHT - HEIGHT / 10 * multiply + 100))
	{
		switch (multiply)
		{
		case 6:
			*idk = NEW_GAME;
			break;
		case 10:
			*idk = RANK;
			break;
		case 20:
			*idk = OPTIONS;
			break;
		case 30:
			*idk = EXIT;
			break;
		}
	}
}


void menu
(
	ALLEGRO_EVENT_QUEUE	 **queue,
	ALLEGRO_DISPLAY **window,
	int *idk
)
{

	ALLEGRO_BITMAP *title = NULL;
	ALLEGRO_BITMAP *key[4];
	ALLEGRO_FONT *menufont = NULL;
	bool refresh = true;

	key[0] = al_create_bitmap(250, 65); //czwarty
	key[1] = al_create_bitmap(250, 65);
	key[2] = al_create_bitmap(250, 65);
	key[3] = al_create_bitmap(250, 65);


	if (!initMenu(&title, &menufont))
	{
		return;
	}


	al_set_target_bitmap(key[1]);
	al_clear_to_color(al_map_rgb(20, 20, 20));

	al_set_target_bitmap(key[2]);
	al_clear_to_color(al_map_rgb(20, 20, 20));
	al_set_target_bitmap(al_get_backbuffer(*window));

	while (true)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(*queue, &event);


		if (event.type == ALLEGRO_EVENT_TIMER)
		{
			refresh = true;

		}
		if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			*idk = EXIT;
			break;
		}
		else if (event.type == ALLEGRO_EVENT_MOUSE_AXES || event.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
		{
			lightKey(&event, &key[0], window, 6);
			lightKey(&event, &key[1], window, 5);
			lightKey(&event, &key[2], window, 3);
			lightKey(&event, &key[3], window, 1);
		}
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (event.mouse.button == 1)
			{
				menuOptions(&event, &key[0], 6, idk);
				menuOptions(&event, &key[1], 5, idk);
				menuOptions(&event, &key[2], 3, idk);
				menuOptions(&event, &key[3], 1, idk);

				return;
			}
		}
		if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
			{
				*idk = EXIT;
				break;
			}
		}
		if (refresh && al_event_queue_is_empty(*queue))
		{
			refresh = false;
			al_clear_to_color(al_map_rgb(0, 0, 0));
			//rysowanie
			al_draw_bitmap(title, 0, 0, 0);
			al_draw_bitmap(key[0], WIDTH / 2 - al_get_bitmap_width(key[0]) / 2, 320, 0);
			al_draw_bitmap(key[1], WIDTH / 2 - al_get_bitmap_width(key[1]) / 2, 390, 0);
			al_draw_bitmap(key[2], WIDTH / 2 - al_get_bitmap_width(key[2]) / 2, 460, 0);
			al_draw_bitmap(key[3], WIDTH / 2 - al_get_bitmap_width(key[3]) / 2, 530, 0);

			al_draw_text(menufont, al_map_rgb(255, 255, 255), WIDTH / 2, 320, ALLEGRO_ALIGN_CENTRE, "NOWA GRA");
			al_draw_text(menufont, al_map_rgb(255, 255, 255), WIDTH / 2, 390, ALLEGRO_ALIGN_CENTRE, "RANKING");
			al_draw_text(menufont, al_map_rgb(255, 255, 255), WIDTH / 2, 460, ALLEGRO_ALIGN_CENTRE, "OPCJE");
			al_draw_text(menufont, al_map_rgb(255, 255, 255), WIDTH / 2, 530, ALLEGRO_ALIGN_CENTRE, "WYJSCIE");
			al_flip_display();
		}
	}
	destroyMenu(&title, &menufont);
}